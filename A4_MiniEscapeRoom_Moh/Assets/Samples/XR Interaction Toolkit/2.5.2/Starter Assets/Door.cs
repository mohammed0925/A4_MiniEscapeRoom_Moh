using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public float openAngle = 90f; // Angle to open the door
    public float closeAngle = 0f; // Angle to close the door
    public float openSpeed = 2f; // Speed to open the door

    private Quaternion openRotation; // Rotation for the open door position
    private Quaternion closeRotation; // Rotation for the closed door position
    private bool isOpen = false; // Flag to check if the door is open

    private void Start()
    {
        // Calculate the open and close rotations based on the provided angles
        openRotation = Quaternion.Euler(0, openAngle, 0);
        closeRotation = Quaternion.Euler(0, closeAngle, 0);
    }

    public void DoorOpenClose()
    {
        // Toggle the door state
        isOpen = !isOpen;

        // Determine the target rotation based on the door state
        Quaternion targetRotation = isOpen ? openRotation : closeRotation;

        // Open or close the door smoothly using Quaternion.Lerp
        StartCoroutine(AnimateDoor(targetRotation));
    }

    private IEnumerator AnimateDoor(Quaternion targetRotation)
    {
        float t = 0f;
        Quaternion initialRotation = transform.rotation;

        // Interpolate between the initial and target rotations over time
        while (t < 1f)
        {
            t += Time.deltaTime * openSpeed;
            transform.rotation = Quaternion.Lerp(initialRotation, targetRotation, t);
            yield return null;
        }
    }
}

